/*

var sentences = "javascript"

console.log(sentences[0])
console.log(sentences[4])

var word = "javascript is awesome"
console.log(word.length)

console.log('milan'.charAt(4));
console.log('i am a string'.charAt(3));

var string1 = "I ";
var string2 = "love ";
var string3 = "milan";

console.log(string1.concat(string2, string3));
console.log(string1.concat(string2, string3));

var text = 'we are milanisti'
console.log(text.indexOf(1));

================================================================
var sentence = "I am going to be React Native Developer"; 

var firstWord = sentence.substring(0, 1) ; 
var secondWord = sentence.substring(2, 4) ; 
var thirdWord = sentence.substring(5, 11);
var fourthWord = sentence.substring(11, 14) ;
var fifthWord = sentence.substring(14, 16) ;
var sixthWord = sentence.substring(17, 22) ;
var seventhWord = sentence.substring(23, 29) ;
var eighthWord = sentence.substring(30, 39) ;

var firstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9] ;
var fourthWord = sentence[11] + sentence[12] ; 
var fifthWord = sentence[14] + sentence[15] ;
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] ;
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + 
    sentence[27] + sentence[28] ;
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + 
    sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38] ;

console.log('First Word : ' + firstWord); 
console.log('Second Word : ' + secondWord);
console.log('Third Word : ' + thirdWord); 
console.log('Fourth Word : ' + fourthWord); 
console.log('Fifth Word : ' + fifthWord); 
console.log('Sixth Word : ' + sixthWord); 
console.log('Seventh Word : ' + seventhWord); 
console.log('Eighth Word : ' + eighthWord);
=======================================================================


var sentence2 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 15); 
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
==============================================================================

*/

var sentence3 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence3.substring(0, 3); 
var secondWord2 = sentence3.substring(4, 14); 
var thirdWord2 = sentence3.substring(15, 17);
var fourthWord2 = sentence3.substring(18, 20);
var fifthWord2 = sentence3.substring(21, 25);

var firstWordLength = firstWord2.length
var secondWordLength = secondWord2.length
var thirdWordLength = thirdWord2.length
var fourthWordLength = fourthWord2.length
var fifthWordLength = fifthWord2.length

console.log('First Word: ' + firstWord2 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord2 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord2 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthWord2 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthWord2 + ', with length: ' + fifthWordLength);





// Soal if-else

var nama1 = "jhon"
var peran1 = ""

var nama2 = "jane"
var peran2 = "penyihir"

var nama3 = "Jeanita"
var peran3 = "Guard"

var nama4 = "junaedi"
var peran4 = "werewolf"

if ( nama1 == "" && peran1 == "") {
    console.log("Nama harus diisi!")
} else if (nama1 == "jhon" && peran1 == "" ) {
    console.log("Halo jhon, pilih peranmu untuk memulai misi!")
}
 
if ( nama2 == "" && peran2 == "") {
    console.log("Nama harus diisi!")
} else if (nama2 == "jane" && peran2 == "penyihir" ) {
    console.log("Selamat datang di Dunia Werewolf, Jane. Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}


if ( nama3 == "" && peran3 == "") {
    console.log("Nama harus diisi!")
} else if (nama3 == "Jeanita" && peran3 == "Guard" ) {
    console.log("Selamat datang di Dunia Werewolf, Jeanita. Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}

if ( nama4 == "" && peran4 == "") {
    console.log("Nama harus diisi!")
} else if (nama4 == "junaedi" && peran4 == "werewolf" ) {
    console.log("Selamat datang di Dunia Werewolf, Junaedi. Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}

//========================================================================================
// Soal Switch case

var hari = 20; 
var bulan = 14; 
var tahun = 1945;

if (hari >= 1 && hari <=31 ){
    if (tahun >=1900 && tahun <=2200){

        switch(bulan) {
            case 1:   { console.log(hari + ' Januari ' + tahun); break; }
            case 2:   { console.log(hari + ' Februari ' + tahun); break; }
            case 3:   { console.log(hari + ' Maret ' + tahun); break; }
            case 4:   { console.log(hari + ' April ' + tahun); break; }
            case 5:   { console.log(hari + ' Mei ' + tahun); break; }
            case 6:   { console.log(hari + ' Juni ' + tahun); break; }
            case 7:   { console.log(hari + ' Juli ' + tahun); break; }
            case 8:   { console.log(hari + ' Agustus ' + tahun); break; }
            case 9:   { console.log(hari + ' September ' + tahun); break; }
            case 10:   { console.log(hari + ' Oktober ' + tahun); break; }
            case 11:   { console.log(hari + ' November ' + tahun); break; }
            case 12:   { console.log(hari + ' Desember ' + tahun); break; }
            default:  { console.log('Masukkan bulan yang benar'); }}
    }

} else {
    console.log("Masukkan tanggal yang benar")
}
